# LICENSE Selection and Guide

This repo stores the common licenses which can be used in most projects. This current list is, among other sources, motivated by the template selection on <gitlab.com>.

# Requirements and Installation process

None

# Usage

Download the your needed license file in your own project `LICENSE` folder.

# Contributing

You can add via a merge request licenses which can be found in:

- <https://spdx.org/licenses/>


# Authors/Maintainers and acknowledgment

# License

# Project status

This project is in the start up phase. The following files need to be added:

Source: <https://spdx.org/licenses/>

Popular

- [ ] Apache License 2.0
- [ ] gnu general public license v3.0
- [ ] MIT License

Other

- [ ] affero general public v3
- [x] bsd 2-clause simplified
- [ ] bsd 3-clause new or revised
- [ ] boost software license v1
- [ ] creative commons zero v1 universal
- [ ] eclipse public license 2.0
- [ ] gnu general public license v2
- [ ] gnu lesser general public license 2.1
- [ ] mozilla public license 2.0
- [ ] the unlicense

